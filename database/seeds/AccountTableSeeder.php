<?php

use Illuminate\Database\Seeder;
use App\Account;

class AccountTableSeeder extends Seeder
{
    private $data = [
        [
            'id' => 1,
            'customer_id' => 1,
            'account_number' => '123456',
            'balance' => 1000000,
            'type' => 'primary',
            'description' => '',
        ],
        [
            'id' => 2,
            'customer_id' => 1,
            'account_number' => '654321',
            'balance' => 1000000,
            'type' => 'secondary',
            'description' => '',
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->data as $data) {
            if (is_null(Account::find($data['id']))) {
                $role = Account::create($data);
            }
        }
    }
}
