<?php

use Illuminate\Database\Seeder;
use App\TransactionType;

class TransactionTypesTableSeeder extends Seeder
{
    private $data = [
        [
            'id' => 1,
            'type' => 'deposit',
        ],
        [
            'id' => 2,
            'type' => 'withdraw',
        ],
        [
            'id' => 3,
            'type' => 'transfer send',
        ],
        [
            'id' => 4,
            'type' => 'transfer accept',
        ],
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->data as $data) {
            if (is_null(TransactionType::find($data['id']))) {
                $role = TransactionType::create($data);
            }
        }
    }
}
