<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    private $data = [
        [
            'id' => 1,
            'name' => 'Administrator',
        ],
        [
            'id' => 2,
            'name' => 'Client',
        ],
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->data as $data) {
            if (is_null(Role::find($data['id']))) {
                $role = Role::create($data);
            }
        }
    }
}
