<?php

use Illuminate\Database\Seeder;
use App\Customer;

class CustomersTableSeeder extends Seeder
{
    private $data = [
        [
            'id' => 1,
            'user_id' => 2,
            'pin' => '123456',
            'phone' => '081234567890',
            'address' => 'Jl. Babarsari No 11',
            'status' => 'active',
        ],
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->data as $data) {
            if (is_null(Customer::find($data['id']))) {
                $role = Customer::create($data);
            }
        }
    }
}
