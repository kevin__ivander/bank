<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{

    private $data = [
        [
            'id' => 1,
            'name' => 'Administrator',
            'email' => 'administrator@gmail.com',
            'email_verified_at' => '2018-10-20',
            'role_id' => 1,
            'password' => 'admin',
        ],
        [
            'id' => 2,
            'name' => 'Client',
            'email' => 'client@gmail.com',
            'email_verified_at' => '2018-10-20',
            'role_id' => 2,
            'password' => 'client',
        ],
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->data as $data) {
            if (is_null(User::find($data['id']))) {
                $data['password'] = app('hash')->make($data['password']);
                User::create($data);
            }
        }
    }
}
