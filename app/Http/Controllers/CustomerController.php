<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use App\User;
use DB;
use Validator;

class CustomerController extends Controller
{
    /**
     * Rules for storing new customer.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'user_id' => 'required',
            'pin' => 'required|min:6|max:6',
            'phone' => 'required',
            'address' => 'required',
        ];
    }

    /**
     * Rules for update a customer.
     *
     * @return array
     */
    protected function updateRules()
    {
        return [
            'user_id' => 'required',
            'phone' => 'nullable',
            'address' => 'nullable',
        ];
    }
    
    /**
     * Rules for change customer's pin.
     *
     * @return array
     */
    protected function changePinRules()
    {
        return [
            'user_id' => 'required',
            'pin' => 'required|min:6|max:6',
            'pin_conf' => 'required|same:pin',
        ];
    }

    /**
     * Rules for activate customer.
     *
     * @return array
     */
    protected function activateRules()
    {
        return [
            'status' => 'required',
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Customer::orderBy('id', 'desc')->with('user')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Change pin customer.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function changePin(Request $request)
    {
        $customer = Customer::find($request->id);

        if (is_null($customer)) {
            return response()->json('customer_not_found', 404);
        }

        $this->validate($request, $this->changePinRules());

        try {

            DB::transaction(function () use ($request, &$customer) {
                $customer->update([
                    'pin' => $request->pin,
                ]);
            });
            $result = [
                'status' => 'true',
                'status_code' => 200,
                'message' => 'Change Pin Success',
                'info' => $customer
            ];

            return response()->json(
                $result,
                200
            );
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * Store a newly created customer in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->rules());

        try {
            $user = User::find($request->user_id);

            if (is_null($user)) {
                throw new \Exception('user_not_found');
            }

            if (! is_null($user->customer)) {
                throw new \Exception('user_already_taken');
            }

            DB::transaction(function () use ($request, &$customer) {
                $customer = Customer::create([
                    'user_id' => $request->user_id,
                    'pin' => bcrypt($request->pin),
                    'phone' => $request->phone,
                    'address' => $request->address,
                    'status' => 'not active',
                ]);
            });
            $result = [
                'status' => 'true',
                'status_code' => 201,
                'message' => 'Add Customer Success',
                'info' => $customer
            ];

            return response()->json(
                $result,
                201
            );
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $customer = Customer::find($request->id);

        if (is_null($customer)) {
            return response()->json('customer_not_found', 404);
        }

        $this->validate($request, $this->rules());

        try {
            $user = User::find($request->get('user_id'));

            if (is_null($user)) {
                throw new \Exception('user_not_found');
            }

            DB::transaction(function () use ($request, &$customer) {
                $customer->update([
                    'phone' => $request->phone,
                    'address' => $request->address,
                ]);
            });
            $result = [
                'status' => 'true',
                'status_code' => 200,
                'message' => 'Store Customer Success',
                'info' => $customer
            ];

            return response()->json(
                $result,
                200
            );
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * Activate the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activatedAccount(Request $request)
    {
        $customer = Customer::find($request->id);

        if (is_null($customer)) {
            return response()->json('customer_not_found', 404);
        }

        $this->validate($request, $this->activateRules());

        try {

            DB::transaction(function () use ($request, &$customer) {
                $customer->update([
                    'status' => 'active',
                ]);
            });
            $result = [
                'status' => 'true',
                'status_code' => 200,
                'message' => 'Activate Customer Success',
                'info' => $customer
            ];

            return response()->json(
                $result,
                200
            );
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = Customer::find($id);

        if (is_null($customer)) {
            return response()->json('customer_not_found', 404);
        }

        try {
            DB::transaction(function () use ($customer) {
                $customer->delete();
            });

            return response()->json();
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }
}
