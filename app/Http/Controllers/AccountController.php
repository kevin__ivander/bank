<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Account;
use App\Customer;
use DB;
use Validator;

class AccountController extends Controller
{
    /**
     * Rules for storing new Account.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'customer_id' => 'required',
            'account_number' => 'required|unique:accounts,account_number',
            'balance' => 'required',
            'type' => 'required',
            'description' => 'nullable',
        ];
    }
    /**
     * Display a listing of account.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(auth()->user()->role->id == 1) {
            return Account::with('customer.user')->get();
        }
        else {
            return Account::with('customer.user')->where('customer_id',auth()->user()->customer->id)->get();
        }
    }

    /**
     * Display a listing of customer's account.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCustomerAccount(Request $request)
    {
        return Account::where('customer_id',$request->customer_id)->get();
    }

    /**
     * Display a listing of customer's account.
     *
     * @return \Illuminate\Http\Response
     */
    public function getSumBalanceAccount(Request $request)
    {
        return Account::sum('balance');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store account created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->rules());

        try {
            DB::transaction(function () use ($request, &$user) {
                $account = Account::create([
                    'customer_id' => $request->customer_id,
                    'account_number' => $request->account_number,
                    'balance' => $request->balance,
                    'type' => $request->type,
                    'description' => $request->description,
                ]);
            });
            $result = [
                'status' => 'true',
                'status_code' => 201,
                'message' => 'Add Account Success',
                'info' => $account
            ];

            return response()->json(
                $result,
                201
            );
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $account = Account::find($id);

        if (is_null($account)) {
            return response()->json('account_not_found', 404);
        }

        $this->validate($request, $this->rules());

        try {
            $customer = Customer::find($request->get('customer_id'));

            if (is_null($customer)) {
                throw new \Exception('customer_not_found');
            }

            DB::transaction(function () use ($request, &$account) {
                $account->update([
                    'type' => $request->type,
                    'description' => $request->description,
                ]);
            });
            $result = [
                'status' => 'true',
                'status_code' => 200,
                'message' => 'Update Account Success',
                'info' => $account
            ];

            return response()->json(
                $result,
                200
            );
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $account = Account::find($id);

        if (is_null($account)) {
            return response()->json('account_not_found', 404);
        }

        try {
            DB::transaction(function () use ($account) {
                $account->delete();
            });

            return response()->json();
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }
}
