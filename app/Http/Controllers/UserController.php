<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;
use Validator;

class UserController extends Controller
{
    /**
     * Rules for storing new user.
     *
     * @return array
     */
    protected function storeRules()
    {
        return [
            'role_id' => 'required',
            'name' => 'required',
            'email' => 'required|unique:users,email',
            'password' => 'required',
            'password_conf' => 'required|same:password',
        ];
    }

    /**
     * Rules for updating user.
     *
     * @return array
     */
    protected function updateRules($id)
    {
        return [
            'role_id' => 'required',
            'email' => 'required|unique:users,email,' . $id,
        ];
    }
    
    /**
     * Display a listing of the User.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(auth()->user()->role_id == 1) {
            return User::with('role')->get();
        }
        return auth()->user();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // 
    }

    /**
     * Store a newly created User in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
            'role_id' => 'required',
            'name' => 'required', 
            'email' => 'required|unique:users,email', 
            'password' => 'required', 
            'password_conf' => 'required|same:password', 
        ]);

        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401); 
        }

        try {
            DB::transaction(function () use ($request, &$user) {
                $user = User::create([
                    'role_id' => $request->role_id,
                    'name' => $request->name,
                    'email' => $request->email,
                    'password' => app('hash')->make($request->password),
                ]);
            });
            $result = [
                'status' => 'true',
                'status_code' => 201,
                'message' => 'Add User Success',
                'info' => $user
            ];

            return response()->json(
                $result,
                201
            );
        } catch (Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * Display the specified User.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);

        if (is_null($user)) {
            return response()->json('user_not_found', 404);
        }
        $result = [
            'status' => 'true',
            'status_code' => 201,
            'message' => 'Show User Success',
            'info' => $user
        ];

        return $result;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified User in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, $this->updateRules($id));
        
        $user = User::find($id);

        if (is_null($user)) {
            return response()->json('user_not_found', 404);
        }

        try {
            DB::transaction(function () use ($request, $user) {
                $user->update([
                    'role_id' => $request->role_id,
                    'email' => $request->email,
                ]);
            });

            return response()->json(
                $user,
                200
            );
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * Remove the specified User from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);

        if (is_null($user)) {
            return response()->json('user_not_found', 404);
        }

        try {
            DB::transaction(function () use ($user) {
                $user->delete();
            });

            return response()->json();
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }
}
