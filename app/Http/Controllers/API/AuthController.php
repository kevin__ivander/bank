<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\User;
use App\Customer;
use Auth;
use Hash;
use Validator;

class AuthController extends Controller
{
    private $successStatus = 200;

    public function login()
    {
        // Check if a user with the specified email exists
        $user = User::whereEmail(request('email'))->first();

        if (!$user) {
            return response()->json([
                'message' => 'Wrong email or password',
                'status' => 422
            ], 422);
        }

        // If a user with the email was found - check if the specified password
        // belongs to this user
        if (!Hash::check(request('password'), $user->password)) {
            return response()->json([
                'message' => 'Wrong email or password',
                'status' => 422
            ], 422);
        }

        // If a user is customer
        if($user->role->name == 'Client') {
            if($user->customer->status == 'not active') {
                return response()->json([
                    'message' => 'You must wait for account confirmation',
                    'status' => 401
                ], 401);
            }
        }
        
        // Send an internal API request to get an access token
        $client = DB::table('oauth_clients')
            ->where('password_client', true)
            ->first();

        // Make sure a Password Client exists in the DB
        if (!$client) {
            return response()->json([
                'message' => 'Laravel Passport is not setup properly.',
                'status' => 500
            ], 500);
        }

        $data = [
            'grant_type' => 'password',
            'client_id' => $client->id,
            'client_secret' => $client->secret,
            'username' => request('email'),
            'password' => request('password'),
        ];

        $request = Request::create('/oauth/token', 'POST', $data);

        $response = app()->handle($request);
        // Check if the request was successful
        if ($response->getStatusCode() != 200) {
            return response()->json([
                'message' => 'Wrong email or password',
                'status' => 422
            ], 422);
        }
        
        // Get the data from the response
        $data = json_decode($response->getContent());

        // Format the final response in a desirable format
        return response()->json([
            'token' => $data->access_token,
            'user' => $user,
            'status' => 200
        ]);
    }

    /**
     * Register new user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'email' => 'required|email', 
            'pin' => 'required',
            'pin_conf' => 'required|same:pin',
            'password' => 'required', 
            'password_conf' => 'required|same:password', 
        ]);
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        $user = User::create([
            'name' => request('name'),
            'email' => request('email'),
            'role_id' => 2,
            'password' => bcrypt(request('password'))
        ]);

        $customer = Customer::create([
            'user_id' => $user->id,
            'pin' => bcrypt($request->pin),
            'phone' => $request->phone,
            'address' => $request->address,
            'status' => 'not active',
        ]);

        $result = [
            'status' => 'true',
            'status_code' => 201,
            'message' => 'Add User Success',
            'info' => [
                'user' => $user,
                'customer' => $customer
            ]
        ];

        return response()->json($result, 201); 
    }

    public function logout()
    {
        $accessToken = auth()->user()->token();

        $refreshToken = DB::table('oauth_refresh_tokens')
            ->where('access_token_id', $accessToken->id)
            ->update([
                'revoked' => true
            ]);

        $accessToken->revoke();

        return response()->json(['status' => 200]);
    }
}
