<?php

namespace App\Http\Controllers;

use App\Transaction;
use App\Customer;
use App\Account;
use Validator;
use DB;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    /**
     * Rules for storing new Deposit & Withdraw.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'account_number' => 'required',
            'amount' => 'required',
            'pin' => 'required',
        ];
    }

    /**
     * Rules for storing new Transfer.
     *
     * @return array
     */
    protected function transferRules()
    {
        return [
            'account_number_accept' => 'required',
            'account_number_sender' => 'required',
            'amount' => 'required',
            'pin' => 'required',
        ];
    }

    /**
     * Display a listing of the transaction.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Transaction::with('type')->with('customer.user')->orderBy('id', 'desc')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Create transaction deposit for customer.
     *
     * @return \Illuminate\Http\Response
     */
    public function deposit(Request $request)
    {
        $this->validate($request, $this->rules());

        $account = Account::select('account_number')->where('account_number',$request->account_number)->first();
        if ($account == null) {
            $message = [
                'message' => 'account not found'
            ];
            return response()->json($message, 404);
        }
        $customer = Customer::find(auth()->user()->customer->id);

        if ($customer->pin != $request->pin) {
            $message = [
                'message' => 'pin not valid'
            ];
            return response()->json($message, 404);
        }

        try {
            DB::transaction(function () use ($request, &$transaction) {
                $transaction = Transaction::create([
                    'customer_id' => auth()->user()->customer->id,
                    'account_number' => $request->account_number,
                    'amount' => $request->amount,
                    'type_id' => 1,
                ]);
            });
            
            $account = Account::where('account_number',$request->account_number)->first();
            DB::transaction(function () use ($request, &$account) {
                $total = $account->balance + $request->amount;
                $account->update([
                    'balance' => $total,
                ]);
            });
            
            $result = [
                'status' => 'true',
                'status_code' => 201,
                'message' => 'Add Deposit Success',
                'account' => $account,
                'info' => $transaction
            ];

            return response()->json(
                $result,
                201
            );
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * Create transaction withdraw for customer.
     *
     * @return \Illuminate\Http\Response
     */
    public function withdraw(Request $request)
    {
        $this->validate($request, $this->rules());
        $account = Account::select('account_number')->where('account_number',$request->account_number)->first();

        if (is_null($account)) {
            $message = [
                'message' => 'account not found'
            ];
            return response()->json($message, 404);
        }

        $customer = Customer::find(auth()->user()->customer->id);

        if ($customer->pin != $request->pin) {
            $message = [
                'message' => 'pin not valid'
            ];
            return response()->json($message, 404);
        }
        try {
            DB::transaction(function () use ($request, &$transaction) {
                $transaction = Transaction::create([
                    'customer_id' => auth()->user()->customer->id,
                    'account_number' => $request->account_number,
                    'amount' => $request->amount,
                    'type_id' => 2,
                ]);
            });
            $account = Account::where('account_number',$request->account_number)->first();
            if($account->balance < $request->amount) {
                $message = [
                    'message' => 'not enough balance'
                ];
                return response()->json($message, 400);
            }
            DB::transaction(function () use ($request, &$account) {
                $account->update([
                    'balance' => $account->balance - $request->amount,
                ]);
            });
            $result = [
                'status' => 'true',
                'status_code' => 201,
                'message' => 'Add Deposit Success',
                'account' => $account,
                'info' => $transaction
            ];

            return response()->json(
                $result,
                201
            );
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * Create transaction transfer for customer.
     *
     * @return \Illuminate\Http\Response
     */
    public function transfer(Request $request)
    {
        $this->validate($request, $this->transferRules());

        if ($request->account_number_sender == $request->account_number_accept) {
            $message = [
                'message' => 'account cannot be same'
            ];
            return response()->json($message, 400);
        }

        $account_sender = Account::select('account_number')->where('account_number',$request->account_number_sender)->first();
        if (is_null($account_sender)) {
            $message = [
                'message' => 'account not found'
            ];
            return response()->json($message, 404);
        }

        $account_accept = Account::select('account_number')->where('account_number',$request->account_number_accept)->first();
        if (is_null($account_accept)) {
            $message = [
                'message' => 'account not found'
            ];
            return response()->json($message, 404);
        }

        $customer = Customer::find(auth()->user()->customer->id);
        if ($customer->pin != $request->pin) {
            $message = [
                'message' => 'pin not valid'
            ];
            return response()->json($message, 404);
        }

        try {
            DB::transaction(function () use ($request, &$transaction_send) {
                $transaction_send = Transaction::create([
                    'customer_id' => auth()->user()->customer->id,
                    'account_number' => $request->account_number_sender,
                    'amount' => $request->amount,
                    'type_id' => 3,
                ]);
            });
            DB::transaction(function () use ($request, &$transaction_accept) {
                $transaction_accept = Transaction::create([
                    'customer_id' => auth()->user()->customer->id,
                    'account_number' => $request->account_number_accept,
                    'amount' => $request->amount,
                    'type_id' => 4,
                ]);
            });

            $account = Account::where('account_number',$request->account_number_sender)->first();
            if($account->balance < $request->amount) {
                $message = [
                    'message' => 'not enough balance'
                ];
                return response()->json($message, 400);
            }
            DB::transaction(function () use ($request, &$account) {
                $account->update([
                    'balance' => $account->balance - $request->amount,
                ]);
            });

            $account = Account::where('account_number',$request->account_number_accept)->first();
            DB::transaction(function () use ($request, &$account) {
                $balance = $account->balance;
                $account->update([
                    'balance' => $balance + $request->amount,
                ]);
            });
            $result = [
                'status' => 'true',
                'status_code' => 201,
                'message' => 'Transfer Success',
                'account' => $account,
                'info' => [
                    'transaction_send' => $transaction_send,
                    'transaction_accept' => $transaction_accept
                ]
            ];

            return response()->json(
                $result,
                201
            );
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * Display a listing of customer's mutation.
     *
     * @return \Illuminate\Http\Response
     */
    public function mutationReport(Request $request)
    {
        return Transaction::with('type')
                            ->with('customer.user')
                            ->orderBy('id', 'desc')
                            ->whereBetween('created_at', [$request->date_start, $request->date_end])
                            ->where('customer_id',auth()->user()->customer->id)
                            ->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction)
    {
        //
    }
}
