import currencyFormatter from 'currency-formatter'

export const accesses = {
  MASTER_DATA_MANAGEMENT: 1,
  PURCHASE: 2,
  STOCK_CONTROL: 3,
  POS: 4,
  TRANSACTION_DISCOUNT: 5
}

export function asCurrency (number) {
  return currencyFormatter.format(number, {
    code: 'IDR',
    symbol: 'Rp ',
    thousandSeparator: '.',
    decimalSeparator: ','
  })
}
