import VueRouter from 'vue-router';

let routes = [
    {
        path: '/',
        component: require('./views/Home.vue')
    },
    {
        path: '/login',
        component: require('./views/auth/Login.vue')
    },
    {
        path: '/register',
        component: require('./views/auth/Register.vue')
    },
    {
        path: '/dashboard',
        component: require('./views/Dashboard.vue'),
        meta: { middlewareAuth: true }
    },
    {
        path: '/users',
        component: require('./views/User/Index.vue'),
        meta: { middlewareAuth: true }
    },
    {
        path: '/users/add',
        component: require('./views/User/Create.vue'),
        meta: { middlewareAuth: true }
    },
    {
        path: '/users/update',
        component: require('./views/User/Update.vue'),
        meta: { middlewareAuth: true }
    },
    {
        path: '/transactions',
        component: require('./views/Transaction/Index.vue'),
        meta: { middlewareAuth: true }
    },
    {
        path: '/transactions/add',
        component: require('./views/Transaction/Create.vue'),
        meta: { middlewareAuth: true }
    },
    {
        path: '/transactions/deposit',
        component: require('./views/Transaction/Deposit.vue'),
        meta: { middlewareAuth: true }
    },
    {
        path: '/transactions/withdraw',
        component: require('./views/Transaction/Withdraw.vue'),
        meta: { middlewareAuth: true }
    },
    {
        path: '/transactions/transfer',
        component: require('./views/Transaction/Transfer.vue'),
        meta: { middlewareAuth: true }
    },
    {
        path: '/transactions/mutation',
        component: require('./views/Transaction/Mutation.vue'),
        meta: { middlewareAuth: true }
    },
    {
        path: '/transactions/mutation/mutationresult',
        component: require('./views/Transaction/MutationResult.vue'),
        meta: { middlewareAuth: true }
    },
    {
        path: '/transactions/update',
        component: require('./views/Transaction/Update.vue'),
        meta: { middlewareAuth: true }
    },
    {
        path: '/customers',
        component: require('./views/Customer/Index.vue'),
        meta: { middlewareAuth: true }
    },
    {
        path: '/customers/add',
        component: require('./views/Customer/Create.vue'),
        meta: { middlewareAuth: true }
    },
    {
        path: '/customers/update',
        component: require('./views/Customer/Update.vue'),
        meta: { middlewareAuth: true }
    },
    {
        path: '/accounts',
        component: require('./views/Account/Index.vue'),
        meta: { middlewareAuth: true }
    },
    {
        path: '/accounts/add',
        component: require('./views/Account/Create.vue'),
        meta: { middlewareAuth: true }
    },
    {
        path: '/accounts/update',
        component: require('./views/Account/Update.vue'),
        meta: { middlewareAuth: true }
    }
];

const router = new VueRouter({
    routes
});

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.middlewareAuth)) {                
        if (!auth.check()) {
            next({
                path: '/login',
                query: { redirect: to.fullPath }
            });

            return;
        }
    }

    next();
})

export default router;