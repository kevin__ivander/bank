<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/register', 'API\AuthController@register');
Route::post('login', [ 'as' => 'login', 'uses' => 'API\AuthController@login']);

Route::middleware('auth:api')->group(function () {
    Route::post('/logout', 'API\AuthController@logout');

    Route::resource('users', 'UserController');
    Route::resource('roles', 'RoleController');
    Route::resource('accounts', 'AccountController');
    Route::resource('customers', 'CustomerController');
    Route::resource('types', 'TransactionTypeController');
    Route::resource('transactions', 'TransactionController');

    Route::get('/get-customer-account', 'AccountController@getCustomerAccount');
    Route::get('/get-sum-balance', 'AccountController@getSumBalanceAccount');

    Route::post('/deposit', 'TransactionController@deposit');
    Route::post('/withdraw', 'TransactionController@withdraw');
    Route::post('/transfer', 'TransactionController@transfer');
    Route::post('/mutationReport', 'TransactionController@mutationReport');
});
